Welcome to AIOULINUX
Distribution Name: AiouLinux

Goal: 
Make a Linux distribution that is focused on the development of Arduino and 
IOT. This distribution must be in "Live" format and fully functional. 
If the user likes the distribution it is possible to do a final installation. 
The license is in GNU format totally free and free for use in any segment 
whether personal, academic or commercial.
Name of the Author of the Distro: CÇsar A. Massari 
cesar.massari@gmail.com 
LinkedIn:https://www.linkedin.com/in/cesarmassari/
 
How to install and use:

Start the login:

user admin: aioulinux 
password: Arduino

user root 
password: arduino

OBS: I highly recommend you change this password. Just command: passwd.

For use like LIVE CD: 
Just record this image from pendrive or cd with boot mode, in boot select fist 
option. On logon use admin password. 
programs for record boot image in cd: 
- nero 
- image iso recorder 
- etc...

for record image in pendrive like bootable: 
Just record this image with bootable mode on stick and select fisrt option on 
boot. On logon use admin password. 
- UNetBootIn 
- etc.. 

If you use a Virtual Machine, just set Image ISO to boot on start. 
Just select fisrt option on boot. On logon use admin password.


For a definitive installation on computer 
Warning: this can format your computer and erase all data. This is 
recommended for new computers. 
Just select second option and follow the instructions.
If need Help, write me! 
cesar.massari@gmail.com
 
If you liked, donate in one of this:
Bitcoin ..............: 
Bitcoin cash ......: 
Ethereum .........: 
ZCash ................: 
SmartCash ........:
If you donate, please send us an email confirming.
OBS: 
Donations worth more than 0.5 Bitcoin can choose which software will be part of 
the next distribution. Whenever the value exceeds 1 Bitcoin a new version will 
be released.
 

 
Software present on this Distro:
*	Arduino IDE with ArduBlocks
*	Eclipse C++ with Arduino Plugin
*	Visual Studio Code with Arduino Plugin
*	Fritzing circuit designer and simulator
*	Cura 3d Printer
*	Cura2 3d Printer
*	Repetier Host 3d printer
*	Gimp - Image editor
*	Postman - call APIs
*	Eagle - computer feature
*	Firefox
*	File manager PCManFM
*	GIT
*	GitKraken Client app
*	IDLE3
*	Python3
*	Inkscape
*	Java8
*	LibreCad
*	FreeCad
*	Blender
*	Mate Disk Usage Analyzer
*	Mate System Monitor
*	MC - Midnight Commander
*	QTransmission Bittorrent Client
*	System Profiler and Benchmark
*	Task Manager
*	Open Office
*	VLC
*	Text Editor
*	XAMPP - Apache server with PHP, SFTP Server, MySql server database
*	Games
*	Dislocker (for hack bitlocker)
 

 
Build Features: 
Distro Base: Debian 9.5 amd 64bit 
Size: 4Gb 
Xwindow manager: Mate, XFCE
